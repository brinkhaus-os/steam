# datagate

# Overview

datagate is an application which reads from input sources and sends the data to various endpoints via different protocols such as MQTT and gRPC. It is capable of sampling and processing from a single source and sending the processed data samples to multiple endpoints via MQTT and gRPC or save locally as a csv file.

It is aligned to the principles of
- Data sovereignty (complies with rights on data)
- Efficiency and speed (coded in C++, multi-threaded architecture)
- Near real-time-capabitilies (supports gRPC live streaming of data)
- Ease of use (supports MQTT data distribution)

In the standard configuration, datagate acts as the gateway service by reading data from sources and by sending that data onto the Data Switch, which will then provide data to the GaiaX-network or in regular clouds.

# Introduction
You will find the introduction about datagate in this wiki's sidebar under [Section 1](https://gitlab.com/brinkhaus-os/datagate/-/wikis/1.-Overview/1.1-Introduction).  

# Technical Details
You will find the technical details in this wiki's sidebar under [Section 2](https://gitlab.com/brinkhaus-os/datagate/-/wikis/2.-Technical-Details/2.1-System-Architecture).  
Details about the System architecture, a Runtime Tests system and the REST interface available are discussed in this section.

# Installation and Usage
How to install and use datagate is available in this wiki's sidebar under [Section 3.1](https://gitlab.com/brinkhaus-os/datagate/-/wikis/3.-Installation/3.1-Installation-Steps), [Section 3.2](https://gitlab.com/brinkhaus-os/datagate/-/wikis/3.-Installation/3.2-Installation-as-Docker-Containers) and [Section 4](https://gitlab.com/brinkhaus-os/datagate/-/wikis/4.-Usage/4.1-Configuration).  

Section 3.1 and 3.2, which describes Installation, gives an account of the installation as debian packages as well as in the form of Docker containers.
Section 4, which describes Usage, gives an account of the configuration that can be done for running the datagate application, as well as details about retrieving the data via gRPC clients and MQTT. The UI data description subsection gives an account of how the UI can be modified and the UI components that can be set up on the UI.

# Support
Please reach out to brinkhaus@brinkhaus-gmbh.de for any support related queries.
